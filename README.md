Building a new container
------------------------
```
docker build . --tag registry.gitlab.com/hpierce1102/classfinder/php80
```

Debug current WIP container
---------------------------
```
docker run 
-v "C:\Users\HPierce\PhpstormProjects\ClassFinder":/builds/hpierce1102/ClassFinder 
-it registry.gitlab.com/hpierce1102/classfinder/php80 /bin/bash
```

Debugging ClassFinder on specific containers
--------------------------------------------

```
docker run 
-v "C:\Users\HPierce\PhpstormProjects\ClassFinder":/builds/hpierce1102/ClassFinder 
-it registry.gitlab.com/hpierce1102/classfinder/php80 /bin/bash
```
